# dynamicreports-demo


### Info 
This project is created using dynamicJasper library for dynamically generating various types of reports without reaching the jrxml implementation. DynamicJasper is purely java based.

### Types of Reports

1. Basic Reports
2. Chart Reports
3. Group Reports
4. Reflective Reports
5. Sub-Reports