package com.example.dynamicreportsdemo.reportsTypes.subReport;

import ar.com.fdvs.dj.core.DJConstants;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.SubReportBuilder;
import ar.com.fdvs.dj.domain.entities.Subreport;
import com.example.dynamicreportsdemo.dataSource.DataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/sub")
public class SubReport {

    @Autowired
    DataSource dataSource;

    @RequestMapping("/generate")
    public void buildReport() throws Exception {

        FastReportBuilder drb = new FastReportBuilder();
        drb.addColumn("State", "state", String.class.getName(), 30)
                .addColumn("Branch", "branch", String.class.getName(), 30)
                .addColumn("Product Line", "productLine", String.class.getName(), 50)
                .addColumn("Item", "item", String.class.getName(), 50)
                .addColumn("Item Code", "itemCode", String.class.getName(), 30, true)
                .addColumn("Quantity", "quantity", Integer.class.getName(), 60, true)
                .addColumn("Amount", "amount", Integer.class.getName(), 70, true)
                .addGroups(2)
                .setTitle("November 2020 sales report")
                .setSubtitle("This report was generated at " + new Date())
                .setUseFullPageWidth(true);

        /**
         * Create the subreport. Note that the "subreport" object is then passed
         * as parameter to the GroupBuilder
         */
        Subreport subreport = new SubReportBuilder()
                .setDataSource(DJConstants.DATA_SOURCE_ORIGIN_PARAMETER,
                        DJConstants.DATA_SOURCE_TYPE_COLLECTION,
                        "statistics")
                .setDynamicReport(createFooterSubReport(), new ClassicLayoutManager())
                .build();

        drb.addSubreportInGroupFooter(1, subreport);

        /**
         * add in a map the paramter with the data source to use in the subreport.
         * The "params" map is later passed to the DynamicJasperHelper.generateJasperPrint(...)
         */
       // params.put("statistics", Product.statistics_); // the 2nd param is a static Collection
        /**
         * Create the group and add the subreport (as a Fotter subreport)
         */
        drb.setUseFullPageWidth(true);
        DynamicReport dr = drb.build();

        JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), dataSource.getDataSource());
        JasperExportManager.exportReportToPdfFile(jp, "C:\\reports\\sub_report_group.pdf");


    }

    /**
     * Created and compiles dynamically a report to be used as subreportr
     *
     * @return
     * @throws Exception
     */
    private DynamicReport createFooterSubReport() throws Exception {
        FastReportBuilder rb = new FastReportBuilder();
        DynamicReport dr = rb
                .addColumn("Area", "name", String.class.getName(), 100)
                .addColumn("Average", "average", Float.class.getName(), 50)
                .addColumn("%", "percentage", Float.class.getName(), 50)
                .addColumn("Amount", "amount", Float.class.getName(), 50)
                .addGroups(1)
                .setMargins(5, 5, 20, 20)
                .setUseFullPageWidth(true)
                .setTitle("Sub-Report for this group")
                .build();
        return dr;
    }

}
