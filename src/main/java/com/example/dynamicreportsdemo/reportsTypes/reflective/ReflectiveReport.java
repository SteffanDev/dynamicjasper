package com.example.dynamicreportsdemo.reportsTypes.reflective;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ReflectiveReportBuilder;
import ar.com.fdvs.dj.util.SortUtils;
import com.example.dynamicreportsdemo.dataSource.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/reflective")
public class ReflectiveReport {

    @Autowired
    DataSource dataSource;

    /**
     * Test N� 1. With only the collection, the ReflectiveReportBuilder make some guesses
     */

    @RequestMapping("/guessed")
    public void testReport() {

        DynamicReport dynamicReport = new ReflectiveReportBuilder( dataSource.getDataCollection()).build();
        doReport(dynamicReport,  dataSource.getDataCollection(), "");
    }

    /**
     * Test N�2, the same but we tell the builder the order of the columns, we also add 3 groups
     */

    @RequestMapping("/ordered")
    public void testOrderedReport() {
        final List items = SortUtils.sortCollection(dataSource.getDataCollection(), Arrays.asList(new String[]{"productLine", "item", "state"}));
        String[] columOrders = new String[]{"productLine", "item", "state", "itemCode", "branch", "quantity", "amount"};
        DynamicReport dynamicReport = new ReflectiveReportBuilder(items, columOrders).addGroups(3).build();
        doReport(dynamicReport, items, "ordered");
    }

    public void doReport(final DynamicReport _report, final Collection _data, String name) {
        try {
            final JasperPrint jasperPrint = DynamicJasperHelper.generateJasperPrint(_report, new ClassicLayoutManager(), _data);
            JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\reports\\report_reflective_"+name+".pdf");
        } catch (JRException e) {
            e.printStackTrace();
        }
    }
}
