package com.example.dynamicreportsdemo.reportsTypes.group;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.builders.GroupBuilder;
import ar.com.fdvs.dj.domain.constants.*;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.entities.DJGroup;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import com.example.dynamicreportsdemo.dataSource.DataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

@RestController
@RequestMapping("/group")
public class GroupReportWithImageBanner {

    @Autowired
    DataSource dataSource;

    @RequestMapping("/generate")
    public void buildReport() throws Exception {

          		Style detailStyle = new Style();
          		Style headerStyle = new Style();
          		//headerStyle.setFont(Font.VERDANA_MEDIUM_BOLD);
          		headerStyle.setBorderBottom(Border.PEN_2_POINT());
         		headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
          		headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
          		headerStyle.setBackgroundColor(Color.DARK_GRAY);
          		headerStyle.setTextColor(Color.WHITE);
          		headerStyle.setTransparency(Transparency.OPAQUE);

          		Style titleStyle = new Style();
          		//titleStyle.setFont(new Font(18,Font._FONT_VERDANA,true));
          		Style importeStyle = new Style();
          		importeStyle.setHorizontalAlign(HorizontalAlign.RIGHT);
          		Style oddRowStyle = new Style();
          		oddRowStyle.setBorder(Border.NO_BORDER()); oddRowStyle.setBackgroundColor(Color.LIGHT_GRAY);oddRowStyle.setTransparency(Transparency.OPAQUE);

          		DynamicReportBuilder drb = new DynamicReportBuilder();
          		Integer margin = new Integer(20);
          			drb.setTitleStyle(titleStyle)
          			.setTitle("November 2020 sales report")					//defines the title of the report
          			.setSubtitle("The items in this report correspond "
          					+"to the main products: DVDs, Books, Foods and Magazines")
          			.setDetailHeight(new Integer(15))
          			.setLeftMargin(margin)
          			.setRightMargin(margin)
          			.setTopMargin(margin)
          			.setBottomMargin(margin)
          			.setPrintBackgroundOnOddRows(true)
          			.setOddRowBackgroundStyle(oddRowStyle);
//          			.addFirstPageImageBanner("C:\\Users\\steffan.d\\Downloads\\logo_fdv_solutions_60.jpg", new Integer(197), new Integer(60), ImageBanner.ALIGN_LEFT)
//          			.addFirstPageImageBanner("C:\\Users\\steffan.d\\Downloads\\dynamicJasper.png", new Integer(300), new Integer(60), ImageBanner.ALIGN_RIGHT)
//          			.addImageBanner("C:\\Users\\steffan.d\\Downloads\\logo_fdv_solutions_60.jpg", new Integer(100), new Integer(25), ImageBanner.ALIGN_LEFT, ImageScaleMode.FILL)
//          			.addImageBanner("C:\\Users\\steffan.d\\Downloads\\dynamicJasper.png", new Integer(150), new Integer(25), ImageBanner.ALIGN_RIGHT, ImageScaleMode.FILL);

          		AbstractColumn columnState = ColumnBuilder.getNew().setColumnProperty("state", String.class.getName())
          			.setTitle("State").setWidth(new Integer(85))
          			.setStyle(titleStyle).setHeaderStyle(headerStyle).build();

          		AbstractColumn columnBranch = ColumnBuilder.getNew().setColumnProperty("branch", String.class.getName())
          			.setTitle("Branch").setWidth(new Integer(85))
          			.setStyle(detailStyle).setHeaderStyle(headerStyle).build();

         		AbstractColumn columnaProductLine = ColumnBuilder.getNew().setColumnProperty("productLine", String.class.getName())
         			.setTitle("Product Line").setWidth(new Integer(85))
         			.setStyle(detailStyle).setHeaderStyle(headerStyle).build();

         		AbstractColumn columnaItem = ColumnBuilder.getNew().setColumnProperty("item", String.class.getName())
         			.setTitle("Item").setWidth(new Integer(85))
         			.setStyle(detailStyle).setHeaderStyle(headerStyle).build();

         		AbstractColumn columnCode = ColumnBuilder.getNew().setColumnProperty("itemCode", String.class.getName())
         			.setTitle("ID").setWidth(new Integer(40))
         			.setStyle(importeStyle).setHeaderStyle(headerStyle).build();

         		AbstractColumn columnaQuantity = ColumnBuilder.getNew().setColumnProperty("quantity", Integer.class.getName())
         			.setTitle("Quantity").setWidth(new Integer(80))
         			.setStyle(importeStyle).setHeaderStyle(headerStyle).build();

         		AbstractColumn columnAmount = ColumnBuilder.getNew().setColumnProperty("amount", Integer.class.getName())
         			.setTitle("Amount").setWidth(new Integer(90)).setPattern("$ 0.00")
         			.setStyle(importeStyle).setHeaderStyle(headerStyle).build();


         		GroupBuilder gb1 = new GroupBuilder();
         		DJGroup g1 = gb1.setCriteriaColumn((PropertyColumn) columnState)		//define the criteria column to group by (columnState)
         			.addFooterVariable(columnAmount, DJCalculation.SUM)		//tell the group place a variable in the footer
         																					//of the column "columnAmount" with the SUM of all
         																					//values of the columnAmount in this group.

         			.addFooterVariable(columnaQuantity,DJCalculation.SUM)	//idem for the columnaQuantity column
         			.setGroupLayout(GroupLayout.VALUE_IN_HEADER)				//tells the group how to be shown, there are many
         																					//posibilities, see the GroupLayout for more.
         			.build();

         		GroupBuilder gb2 = new GroupBuilder();										//Create another group (using another column as criteria)
         		DJGroup g2 = gb2.setCriteriaColumn((PropertyColumn) columnBranch)		//and we add the same operations for the columnAmount and
         			.addFooterVariable(columnAmount,DJCalculation.SUM)		//columnaQuantity columns
         			.addFooterVariable(columnaQuantity,DJCalculation.SUM)
         			.build();

         		drb.addColumn(columnState);
         		drb.addColumn(columnBranch);
         		drb.addColumn(columnaProductLine);
         		drb.addColumn(columnaItem);
         		drb.addColumn(columnCode);
         		drb.addColumn(columnaQuantity);
         		drb.addColumn(columnAmount);

         		drb.addGroup(g1);	//add group g1
         		//drb.addGroup(g2);	//add group g2

         		drb.setUseFullPageWidth(true);

        DynamicReport dr = drb.build();

        //JRDataSource ds = new JRBeanCollectionDataSource(getDataSource());
        JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), dataSource.getDataSource());
        JasperExportManager.exportReportToPdfFile(jp, "C:\\reports\\report_group.pdf");

         	}


}
