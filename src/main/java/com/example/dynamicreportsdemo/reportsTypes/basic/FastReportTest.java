package com.example.dynamicreportsdemo.reportsTypes.basic;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DJValueFormatter;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.builders.StyleBuilder;
import com.example.dynamicreportsdemo.dataSource.DataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.*;

@RestController
@RequestMapping("/basic")
public class FastReportTest {

    @Autowired
    DataSource dataSource;

    @RequestMapping("/generate")
    public void buildReport() throws Exception {
        FastReportBuilder drb = new FastReportBuilder();

        drb.setDefaultEncoding("UTF-8");
        Font font = new Font(28,"Iskoola Pota","C:\\Users\\steffan.d\\Downloads\\iskoola_potha_unicode\\iskpota.ttf",
                Font., true);
        Style titleStyle = new StyleBuilder(false).setFont(font).build();

        drb.addColumn("State", "state", String.class.getName(),30)
                .addColumn("Branch", "branch", String.class.getName(),30)
                .addColumn("Product Line", "productLine", String.class.getName(),50)
                .addColumn("Item", "item", String.class.getName(),50)
                .addColumn("Item Code", "itemCode", String.class.getName(),30,true)
                .addColumn("Quantity", "quantity", Integer.class.getName(),60,true)
                .addColumn("Amount", "amount", Integer.class.getName(),70,true)
                .addGroups(2)
                .setTitle("ඩිජිටල් සිංහල යතුරුපුවරුව")
                .setTitleStyle(titleStyle)
                .setSubtitle("This report was generated at " + new Date())
                .setPrintBackgroundOnOddRows(true)
                .setUseFullPageWidth(true)
               .addGlobalFooterVariable(drb.getColumn(4), DJCalculation.COUNT, null, new DJValueFormatter() {
                   public String getClassName() {
                       return String.class.getName();
                   }
                   public Object evaluate(Object value, Map fields, Map variables,   Map parameters) {
                       return (value == null ? "0" : value.toString()) + " Clients";
                   }
               });

        //ඩිජිටල් සිංහල යතුරුපුවරුව

        DynamicReport dr = drb.build();

        //JRDataSource ds = new JRBeanCollectionDataSource(getDataSource());


        JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), dataSource.getDataSource());
        jp.setLocaleCode("si-LK");
        JasperExportManager.exportReportToPdfFile(jp, "C:\\reports\\r_ඩිජිටල් සිංහල යතුරුපුවරුව.pdf");

    }

}
