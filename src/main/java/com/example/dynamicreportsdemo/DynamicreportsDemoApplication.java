package com.example.dynamicreportsdemo;

import com.example.dynamicreportsdemo.dataSource.DataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DynamicreportsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicreportsDemoApplication.class, args);

	}

	@Bean
	public DataSource dataSource(){
		return new DataSource();
	}

}
