package com.example.dynamicreportsdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SalesDto {

    private String state;
    private String branch;
    private String productLine;
    private String item;
    private String itemCode;
    private int quantity;
    private int amount;

}
