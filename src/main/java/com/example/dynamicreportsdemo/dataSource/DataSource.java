package com.example.dynamicreportsdemo.dataSource;

import com.example.dynamicreportsdemo.dto.SalesDto;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import java.util.ArrayList;
import java.util.Collection;


public class DataSource {
    public static JRBeanCollectionDataSource getDataSource() {

        Collection<SalesDto> coll = new ArrayList<>();
        coll.add(new SalesDto("SriLanka","Galle","Garment","T-shirts","GT2020",10,2000));
        coll.add(new SalesDto("SriLanka","Colombo","Plastic","Balls","PB2020",10,2000));
        coll.add(new SalesDto("SriLanka","Galle","Garment","T-shirts","GT2020",10,2000));
        coll.add(new SalesDto("India","Delhi","Garment","Frocks","GF2020",20,3000));
        coll.add(new SalesDto("India","Delhi","Garment","T-shirts","GT2020",200,4500));
        coll.add(new SalesDto("India","Bangalore","Plastic","Balls","PB2020",20,3000));
        coll.add(new SalesDto("India","Tamilnadu","Garment","Dress","DR2020",10,3250));
        coll.add(new SalesDto("Australia","Melbourne","Utensils","Scissors","US2020",10,200));

        return new JRBeanCollectionDataSource(coll);
    }

    public static Collection getDataCollection() {
        Collection<SalesDto> coll = new ArrayList<>();
        coll.add(new SalesDto("SriLanka","Galle","Garment","T-shirts","GT2020",10,2000));
        coll.add(new SalesDto("SriLanka","Colombo","Plastic","Balls","PB2020",10,2000));
        coll.add(new SalesDto("SriLanka","Galle","Garment","T-shirts","GT2020",10,2000));
        coll.add(new SalesDto("India","Delhi","Garment","Frocks","GF2020",20,3000));
        coll.add(new SalesDto("India","Delhi","Garment","T-shirts","GT2020",200,4500));
        coll.add(new SalesDto("India","Bangalore","Plastic","Balls","PB2020",20,3000));
        coll.add(new SalesDto("India","Tamilnadu","Garment","Dress","DR2020",10,3250));
        coll.add(new SalesDto("Australia","Melbourne","Utensils","Scissors","US2020",10,200));

        return coll;
    }
}
